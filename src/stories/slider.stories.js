import React from "react";
import { action } from "@storybook/addon-actions";
import Slider from "../components/slider";


export default {
    title: "Components/Slider",
    component: Slider,
};

const Template = (args) => <Slider {...args} />;

export const Default = Template.bind({});
Default.args = {
    min: 0,
    max: 100,
    step: 1,
    value: 60,
    onChange: action("Slider value changed"),
};