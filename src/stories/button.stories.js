import React from "react";
import { action } from "@storybook/addon-actions";
import Button from "../components/button";

export default {
    title: "Components/Button",
    component: Button,
};

const Template = (args) => <Button {...args} />;

export const Default = Template.bind({});
Default.args = {
    text: "Click me",
    font: "Arial",
    color: "blue",
    size: "16px",
    onClick: action("Button clicked"),
};
