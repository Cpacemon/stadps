import React from "react";
import Alert from "../components/allert";


export default {
    title: "Components/Alert",
    component: Alert,
};

const Template = (args) => <Alert {...args} />;

export const SmallAlert = Template.bind({});
SmallAlert.args = {
    message: "This is a small alert",
    textColor: "black",
    size:"small",
    fontWeight: "normal",
    backgroundColor: "pink",
    borderColor: "gray",
};
export const MediumAlert = Template.bind({});
MediumAlert.args = {
    message: "This is a medium alert",
    textColor: "gray",
    size:"medium",
    fontWeight: "normal",
    backgroundColor: "black",
    borderColor: "red",
};
export const LargeAlert = Template.bind({});
LargeAlert.args = {
    message: "This is a large alert",
    textColor: "black",
    size: "large",
    fontWeight: "bold",
    backgroundColor: "red",
    borderColor: "blue",
};