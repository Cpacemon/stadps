import React from "react";

const Button = ({ text, onClick, font, color, size }) => {
    const buttonStyle = {
        fontFamily: font,
        backgroundColor: color,
        fontSize: size,
        padding: "10px 20px",
        borderRadius: "4px",
        border: "none",
        color: "#fff",
        cursor: "pointer",
    };

    return (
        <button style={buttonStyle} onClick={onClick}>
            {text}
        </button>
    );
};

export default Button;