import React, { useState } from "react";

const Slider = ({ min, max, step, value, onChange }) => {
    const [sliderValue, setSliderValue] = useState(value);

    const handleSliderChange = (event) => {
        const newValue = parseInt(event.target.value, 10);
        setSliderValue(newValue);
        onChange(newValue);
    };

    return (
        <div>
            <input
                type="range"
                min={min}
                max={max}
                step={step}
                value={sliderValue}
                onChange={handleSliderChange}
            />
            <span>{sliderValue}</span>
        </div>
    );
};

export default Slider;