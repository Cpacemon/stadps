import React from "react";

const Alert = ({ message, textColor,size, fontWeight,backgroundColor,borderColor }) => {
    const alertStyle = {
        size: size,
        fontWeight: fontWeight,
        color: textColor,
        backgroundColor: backgroundColor,
        borderColor: borderColor,
    };

    return (
        <div className={`alert ${size}`} style={alertStyle}>
            {message}
        </div>
    );
};

export default Alert;